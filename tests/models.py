from sqlalchemy import Column, ForeignKey, Integer, Text
from sqlalchemy.orm import configure_mappers, declarative_base, relationship


class Base:
    __allow_unmapped__ = True


Base = declarative_base(cls=Base)  # type: ignore


class Author(Base):
    __tablename__ = "author"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)

    def __str__(self):
        return f"Author(name={self.name})"

    __repr__ = __str__


class Book(Base):
    __tablename__ = "book"
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(Text)
    author_id = Column(
        "author_id", Integer, ForeignKey("author.id", ondelete="CASCADE"), nullable=True
    )
    author: Author = relationship(Author, backref="books")


configure_mappers()
