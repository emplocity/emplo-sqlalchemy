from unittest.mock import patch

import pytest
from sqlalchemy import inspect
from sqlalchemy.exc import IntegrityError

from emplo_sqlalchemy.managers import Manager, ManagerError, is_model_joined

from .models import Author, Book


class AuthorManager(Manager):
    model = Author

    def _filter(self, query, **args):
        if args.get("name"):
            query = self._filter_by_name(query, args["name"])

        order_args = self._underscore_order_args(args)
        name_ordering = self._get_name_ordering(order_args)
        if name_ordering:
            query = self._order_by_name(query, name_ordering)
        return query

    def _get_name_ordering(self, order_args):
        return self._get_field_ordering(order_args, "name")

    def _filter_by_name(self, query, name):
        return query.filter(Author.name == name)

    def _order_by_name(self, query, ordering):
        return self._order_by(query, Author.name, ordering)


def test_default_queryset(db_session, author_factory):
    author1 = author_factory(name="Hemingway")
    author2 = author_factory(name="Orwell")
    manager = AuthorManager(db_session)

    assert manager.queryset.all() == [author1, author2]


def test_query_all(db_session, author_factory):
    author1 = author_factory(name="Hemingway")
    author2 = author_factory(name="Orwell")
    manager = AuthorManager(db_session)

    assert manager.all() == [author1, author2]


def test_query_get(db_session, author_factory):
    author1 = author_factory(name="Hemingway", id=3)
    manager = AuthorManager(db_session)
    assert manager.get(author1.id) == author1


def test_query_get_not_found(db_session, author_factory):
    manager = AuthorManager(db_session)
    assert manager.get(4) is None


def test_is_model_joined(db_session, author_factory, book_factory):
    _ = author_factory(name="Hemingway")
    book = book_factory(title="Moby Dick")
    manager = AuthorManager(db_session)
    query = manager.queryset.filter(Book.title == book.title)
    joined_query = query.join(Book)

    assert not is_model_joined(query, Book)
    assert is_model_joined(joined_query, Book)
    assert not manager.is_model_joined(query, Book)
    assert manager.is_model_joined(joined_query, Book)


def test_get_or_create_when_model_doesnt_exist(db_session):
    manager = AuthorManager(db_session)
    author, created = manager.get_or_create(name="Hemingway")
    assert author.id is not None
    assert created is True


def test_get_or_create_when_model_exists(db_session, author_factory):
    author = author_factory(name="Hemingway")
    manager = AuthorManager(db_session)
    existing_author, created = manager.get_or_create(name="Hemingway")
    assert existing_author.id == author.id
    assert created is False


def test_get_or_create_integrity_error(db_session):
    manager = AuthorManager(db_session)
    error = IntegrityError("dummy", None, BaseException())
    with patch.object(db_session, "add", side_effect=error):
        with pytest.raises(ManagerError):
            manager.get_or_create(name="Hemingway")


def test_add_with_kwargs(db_session):
    manager = AuthorManager(db_session)
    author = manager.add(name="Hemingway")
    assert inspect(author).persistent is True


def test_delete(db_session, author_factory):
    author = author_factory(name="Hemingway")
    manager = AuthorManager(db_session)
    manager.delete(author)
    assert inspect(author).deleted is True


def test_filter_by_name(db_session, author_factory):
    _ = author_factory(name="Hemingway")
    author = author_factory(name="Orwell")
    manager = AuthorManager(db_session)
    assert manager.filter(name="Orwell").all() == [author]


def test_order_by_name(db_session, author_factory):
    author1 = author_factory(name="Hemingway")
    author2 = author_factory(name="Orwell")
    manager = AuthorManager(db_session)
    assert manager.filter(orderBy=["name__DESC"]).all() == [author2, author1]
