import pytest
from sqlalchemy import inspect

from emplo_sqlalchemy.scope import transaction_scope

from .models import Author


def test_transaction_scope_should_persist_instance(db_session):
    author = Author(name="Hemingway")
    assert inspect(author).transient is True
    with transaction_scope(db_session):
        db_session.add(author)
        assert inspect(author).pending is True
    assert inspect(author).persistent is True


def test_transaction_scope_rolls_back_on_exception(db_session):
    author = Author(name="Hemingway")
    assert inspect(author).transient is True
    with pytest.raises(ValueError):
        with transaction_scope(db_session):
            db_session.add(author)
            assert inspect(author).pending is True
            raise ValueError("crash")
    assert inspect(author).persistent is False
