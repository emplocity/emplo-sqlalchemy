2.1.1
=====

 - remove obsolete setuptools configuration

2.1.0
=====

 - add support for Python 3.12 and 3.13
 - fix SQLAlchemy 2.0 deprecation warnings

2.0.1
=====

 - backward compatibility with SQLAlchemy 1.4

2.0.0
=====

 - add support for SQLAlchemy 2.0
 - update test_requirements.txt with new versions of dev packages

1.2.3
=====

 - changed typing for Manager class

1.2.2
=====

 - revert deleting __init__.py

1.2.1
=====

 - project setup with pyproject.toml

1.2.0
=====

 - update test_requirements.txt with new versions of dev packages

1.1.0
=====

 - move from using flake8 to ruff
 - update black to version 23.3.0
 - update mypy to version 1.1.1

1.0.0
=====

 - **[BREAKING CHANGE]** ``Manager.get_or_create`` now returns a tuple including
   a boolean flag whether the instance was freshly created
 - drop support for SQLAlchemy 1.3
 - drop support for Python 3.8
 - add support for Python 3.10 and 3.11

0.5.3
=====

 - ordering key can by `order_by` or `orderBy`


0.5.2
=====

 - increase test coverage

0.5.1
=====

 - add typing to Manager.add method


0.5.0
=====

 - add support for SQLAlchemy 1.4

0.4.0
=====

 - remove get_by_gid from Manager implementation.

0.3.0
=====

 - add db statements counter


0.2.0
=====

 - change get_or_create so that it raises an exception upon IntegrityError
   for reasons other than duplicate data
 - add rudimentary tests

0.1.0
=====

 - initial version ported over from emplo-utils
