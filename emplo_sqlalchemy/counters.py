from datetime import datetime, timedelta

import sqlalchemy


class DBStatementCounter:
    """
    Use as a context manager to count the number of execute()'s performed
    against the given sqlalchemy connection.

    Usage:
        with DBStatementCounter(conn) as ctr:
            conn.execute("SELECT 1")
            conn.execute("SELECT 1")


        assert ctr.count == 2
    """

    def __init__(self, conn):
        self.conn = conn
        self.statements = {}
        self.count = 0
        self.total_duration = timedelta()
        # Will have to rely on this since sqlalchemy 0.8 does not support
        # removing event listeners
        self.do_count = False
        self.current_statement_started_at = None
        sqlalchemy.event.listen(conn, "before_execute", self.before_callback)
        sqlalchemy.event.listen(conn, "after_execute", self.after_callback)

    def __enter__(self):
        self.do_count = True
        return self

    def __exit__(self, *_):
        self.do_count = False

    def before_callback(self, *args):
        statement_id = self.count
        statement = str(args[1])
        self.statements[statement_id] = {statement: {"start": datetime.now()}}

    def after_callback(self, *args):
        statement = str(args[1])
        statement_id = self.count
        if self.do_count:
            self.count += 1
            now = datetime.now()
            start = self.statements[statement_id][statement]["start"]
            duration = now - start
            self.statements[statement_id][statement] = {
                "end": str(now),
                "start": str(start),
                "duration": str(duration),
            }
            self.total_duration += duration
