import logging
import time
from contextlib import contextmanager

logger = logging.getLogger(__name__)


@contextmanager
def transaction_scope(session):
    try:
        start = time.perf_counter()
        yield session
        duration = time.perf_counter() - start
        _log_scope_duration("transaction_scope", duration)
        session.commit()
    except Exception:
        session.rollback()
        raise


@contextmanager
def session_scope(db):
    try:
        start = time.perf_counter()
        yield db.session
        duration = time.perf_counter() - start
        _log_scope_duration("session_scope", duration)
    except Exception:
        raise
    finally:
        db.close()


def _log_scope_duration(func_name: str, duration: float) -> None:
    log_level = logging.DEBUG
    stack_info = False
    if 1 < duration < 60:
        log_level = logging.WARNING
    elif duration >= 60:
        log_level = logging.ERROR
        stack_info = True
    logger.log(
        log_level,
        f"{func_name}() took {duration:0.4f} s to execute",
        stack_info=stack_info,
    )
