import re
from typing import Any, ClassVar, Optional

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from sqlalchemy.orm.query import Query

# avoid using declarative_base here, but keep readability of types later on
Model = Any
FileModel = Any


first_cap_re = re.compile("(.)([A-Z][a-z]+)")
all_cap_re = re.compile("([a-z0-9])([A-Z])")


def camel_case_to_underscore(text: str) -> str:
    # copied over from emplo-utils to avoid dependency on that package
    s1 = first_cap_re.sub(r"\1_\2", text)
    return all_cap_re.sub(r"\1_\2", s1).lower()


def is_model_joined(query: Query, model: Model) -> bool:
    if hasattr(query, "_compile_state"):  # SQLAlchemy >= 1.4
        join_entities = query._compile_state()._join_entities  # type: ignore
    else:
        join_entities = query._join_entities  # type: ignore
    if model in [mapper.class_ for mapper in join_entities]:
        return True
    return False


class ManagerError(Exception):
    pass


class SessionManager:
    def __init__(self, session: Session):
        self.session = session


class Manager(SessionManager):
    """
    Base class for SQLAlchemy model managers.

    To use with your model, subclass `emplo.managers.Manager` and set
    `model` class attribute, for example:

    >>> class User(Base):
    ...     # ...
    >>> class UserManager(Manager):
    ...     model = User
    ...     # any custom methods

    See https://docs.djangoproject.com/en/2.2/topics/db/managers/ for a
    similar concept in Django ORM.
    """

    model: ClassVar[Model]

    @property
    def queryset(self) -> Query:
        return self.session.query(self.model)

    def is_model_joined(self, query: Query, model: Model) -> bool:
        return is_model_joined(query, model)

    def all(self) -> list[Model]:
        return self.queryset.all()

    def get(self, object_id: Any) -> Optional[Model]:
        return self.session.get(self.model, object_id)

    def get_or_create(self, **kwargs) -> tuple[Model, bool]:
        instance = self.session.query(self.model).filter_by(**kwargs).first()
        created = False
        if instance is None:
            instance = self.model(**kwargs)
            try:
                self.session.add(instance)
                self.session.flush()
                created = True
            except IntegrityError as e:
                self.session.rollback()
                instance = self.session.query(self.model).filter_by(**kwargs).first()
                # if at this point instance is still None, the IntegrityError
                # wasn't caused by row already existing in the database
                if instance is None:
                    raise ManagerError(
                        "Encountered integrity error for other reason than already existing instance"
                    ) from e
        return instance, created

    def filter(self, query: Optional[Query] = None, **args) -> Query:
        if not query:
            query = self.queryset
        return self._filter(query, **args)

    def _filter(self, query: Query, **args) -> Query:
        raise NotImplementedError

    def _underscore_order_args(self, args: dict[str, list[str]]) -> list[str]:
        order_by = args.get("order_by") or args.get("orderBy") or []
        return [camel_case_to_underscore(x) for x in order_by]

    def _order_by(
        self,
        query: Query,
        order_attribute,
        ordering: Optional[str],
    ) -> Query:
        if ordering and ordering.lower() == "desc":
            order_attribute = order_attribute.desc()
        return query.order_by(order_attribute.nullslast())

    def _get_field_ordering(
        self, order_args: list[str], field_name: str
    ) -> Optional[str]:
        for order_arg in order_args:
            if field_name in order_arg:
                return self._get_ordering(order_arg)
        return None

    def _get_ordering(self, field_name: str) -> Optional[str]:
        default_ordering = "desc"
        possible_ordering = ["asc", "desc"]
        parts = field_name.split("__")
        if len(parts) == 1:
            return default_ordering
        else:
            if parts[1].lower() in possible_ordering:
                return parts[1]
        return None

    def add(self, **kwargs) -> Model:
        instance = self.model(**kwargs)
        self.session.add(instance)
        self.session.flush()
        return instance

    def delete(self, instance: Model) -> None:
        self.session.delete(instance)
        self.session.flush()
